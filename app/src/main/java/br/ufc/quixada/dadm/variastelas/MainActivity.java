package br.ufc.quixada.dadm.variastelas;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import br.ufc.quixada.dadm.variastelas.room.AppDatabase;
import br.ufc.quixada.dadm.variastelas.room.Contato;
import br.ufc.quixada.dadm.variastelas.transactions.Constants;

public class MainActivity extends AppCompatActivity {

    private int selected;
    private ArrayList<Contato> listaContatos;

    private ExpandableListAdapter adapter;
    private ExpandableListView listViewContatos;

    private AppDatabase appDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        selected = -1;
        listaContatos = new ArrayList<Contato>();
        appDatabase = AppDatabase.getInstance(getApplicationContext());

        if(appDatabase.contatoDAO().countContatos() <= 0) {
            appDatabase.contatoDAO().insertAll(new Contato("Contato Teste", "88981000011", "Quixadá"));
        }

        listaContatos.addAll(appDatabase.contatoDAO().getAll());

        adapter = new ExpandableListAdapter(this, listaContatos);


        listViewContatos = (ExpandableListView) findViewById(R.id.expandableListView);
        listViewContatos.setAdapter(adapter);
        listViewContatos.setSelector(android.R.color.holo_blue_light);

        listViewContatos.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v,
                                        int groupPosition, long id) {
                selected = groupPosition;
                return false;
            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.add:
                clicarAdicionar();
                break;
            case R.id.edit:
                clicarEditar();
                break;
            case R.id.delete:
                apagarItemLista();
                break;
            case R.id.settings:
                break;
            case R.id.about:
                break;
        }
        return true;
    }

    private void apagarItemLista() {

        if (listaContatos.size() > 0) {
            Contato contato = listaContatos.get(selected);
            listaContatos.remove(contato);
            appDatabase.contatoDAO().delete(contato);
            adapter.notifyDataSetChanged();
        } else {
            selected = -1;
        }

    }

    public void clicarAdicionar() {
        Intent intent = new Intent(this, ContactActivity.class);
        startActivityForResult(intent, Constants.REQUEST_ADD);
    }

    public void clicarEditar() {

        if (selected != -1  && listaContatos.size() > selected) {
            Intent intent = new Intent(this, ContactActivity.class);

            Contato contato = listaContatos.get(selected);

            intent.putExtra("id", contato.getId());
            intent.putExtra("nome", contato.getNome());
            intent.putExtra("telefone", contato.getTelefone());
            intent.putExtra("endereco", contato.getEndereco());

            startActivityForResult(intent, Constants.REQUEST_EDIT);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Constants.REQUEST_ADD && resultCode == Constants.RESULT_ADD) {

            String nome = (String) data.getExtras().get("nome");
            String telefone = (String) data.getExtras().get("telefone");
            String endereco = (String) data.getExtras().get("endereco");

            Contato contato = new Contato(nome, telefone, endereco);

            long idContato = appDatabase.contatoDAO().insert(contato);

            listaContatos.add(appDatabase.contatoDAO().get(idContato));
            adapter.notifyDataSetChanged();

        } else if (requestCode == Constants.REQUEST_EDIT && resultCode == Constants.RESULT_ADD) {

            String nome = (String) data.getExtras().get("nome");
            String telefone = (String) data.getExtras().get("telefone");
            String endereco = (String) data.getExtras().get("endereco");
            long idEditar = (long) data.getExtras().get("id");

            for (Contato contato : listaContatos) {

                if (contato.getId() == idEditar) {
                    contato.setNome(nome);
                    contato.setEndereco(endereco);
                    contato.setTelefone(telefone);
                    appDatabase.contatoDAO().update(contato);
                }
            }

            adapter.notifyDataSetChanged();

        } else if (resultCode == Constants.RESULT_CANCEL) {
            Toast.makeText(this, "Cancelado", Toast.LENGTH_SHORT).show();
        }

    }

}

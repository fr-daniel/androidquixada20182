package br.ufc.quixada.dadm.variastelas.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ContatoDAO {

    @Query("SELECT * FROM contato")
    List<Contato> getAll();

    @Query("SELECT * FROM contato WHERE id =:id")
    Contato get(long id);

    @Insert
    void insertAll(Contato... contatos);

    @Insert
    long insert(Contato contato);

    @Update
    void update(Contato contato);

    @Delete
    void delete(Contato contato);

    @Query("SELECT COUNT(*) from contato")
    int countContatos();


}
